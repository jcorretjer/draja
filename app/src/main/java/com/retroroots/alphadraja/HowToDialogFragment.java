package com.retroroots.alphadraja;

import android.app.DialogFragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class HowToDialogFragment extends DialogFragment implements GestureDetector.OnGestureListener
{
    private int currentTab = 0;

    private GestureDetectorCompat detector;

    private ImageView ruleImgBtn,
            guideImgBtn,
            howToImg;

    private LinearLayout ruleLayout;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        detector = new GestureDetectorCompat(getActivity(), this);
    }

    public android.app.Dialog onCreateDialog(Bundle savedInstanceState)
    {
        android.app.Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    private boolean isCancelable = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View view = inflater.inflate(R.layout.dialog_fragment_how_to, null);

        setCancelable(isCancelable);

        final Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/edosz.ttf");

        //region Lbls

        final TextView titleLbl = (TextView)view.findViewById(R.id.howToTitleLbl),
                firstRowLbl = (TextView)view.findViewById(R.id.firstRowLbl),
                secondRowLbl = (TextView)view.findViewById(R.id.secondRowLbl),
                thirdRowLbl = (TextView)view.findViewById(R.id.thirdRowLbl);

        titleLbl.setTypeface(tf);

        firstRowLbl.setTypeface(tf);

        secondRowLbl.setTypeface(tf);

        thirdRowLbl.setTypeface(tf);

        //endregion

        ruleLayout = (LinearLayout)view.findViewById(R.id.ruleLayout);

        //region Btns

        final Button closeBtn = (Button)view.findViewById(R.id.closeHowToBtn);

        closeBtn.setTypeface(tf);

        closeBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (((Main)getActivity()).GetIsSoundOn())
                    ((Main) getActivity()).GetSoundObj().PlayFX(R.raw.btnfx, getActivity(), false);

               dismiss();
            }
        });

        ruleImgBtn = (ImageView)view.findViewById(R.id.ruleImgBtn);

                guideImgBtn = (ImageView)view.findViewById(R.id.guideImgBtn);

                howToImg = (ImageView) view.findViewById(R.id.howToImg);

        ruleImgBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Left tab
                switch (currentTab)
                {
                    case 1:

                        currentTab--;

                        howToImg.setVisibility(View.GONE);

                        ruleLayout.setVisibility(View.VISIBLE);

                        //howToImg.setImageResource(R.drawable.how_to_rule_img);

                        ruleImgBtn.setImageResource(R.mipmap.current_tab_img);

                        guideImgBtn.setImageResource(R.mipmap.next_tab_img);

                        break;
                }
            }
        });

        guideImgBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Right tab
                switch (currentTab)
                {
                    case 0:

                        currentTab++;

                        howToImg.setVisibility(View.VISIBLE);

                        ruleLayout.setVisibility(View.GONE);

                        //howToImg.setImageResource(R.drawable.how_to_chart_img);

                        guideImgBtn.setImageResource(R.mipmap.current_tab_img);

                        ruleImgBtn.setImageResource(R.mipmap.next_tab_img);

                        break;
                }
            }
        });

        //endregion

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return detector.onTouchEvent(event);
            }
        });

        return  view;
    }



    @Override
    public boolean onDown(MotionEvent e)
    {
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return true;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e)
    {
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        //Fling left
        if(e1.getX() > e2.getX())
        {
            switch (currentTab)
            {
                case 1:

                    currentTab--;

                    howToImg.setVisibility(View.GONE);

                    ruleLayout.setVisibility(View.VISIBLE);

                    //howToImg.setImageResource(R.drawable.how_to_rule_img);

                    ruleImgBtn.setImageResource(R.mipmap.current_tab_img);

                    guideImgBtn.setImageResource(R.mipmap.next_tab_img);

                    break;
            }
        }

        //Fling right
        else if(e1.getX() < e2.getX())
        {
            switch (currentTab)
            {
                case 0:

                    currentTab++;

                    howToImg.setVisibility(View.VISIBLE);

                    ruleLayout.setVisibility(View.GONE);

                    //howToImg.setImageResource(R.drawable.how_to_chart_img);

                    guideImgBtn.setImageResource(R.mipmap.current_tab_img);

                    ruleImgBtn.setImageResource(R.mipmap.next_tab_img);

                    break;
            }
        }

        return true;
    }


}
